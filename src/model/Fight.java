package model;

import java.util.ArrayList;

public interface Fight {

	public int getModFightSeconds();

	public int getModPauseSeconds();

	public void setModFightSeconds(int value);

	public void setModFightPauseSeconds(int value);

	public Integer getSeconds();

	public void setSeconds(int newValue);

	public void decrementSeconds();

	public Integer getSecondsPause();

	public void setSecondsPause(int secondsPause);

	public void incrementSecondsPause();

	public void decrementSecondsPause();

	public void incrementRound();

	public Integer getRound();

	public Integer getMaxround();

	public int getBluePoints();

	public int getRedPoints();

	public int getBlueWarning();

	public int getRedWarning();

	public void onePointBlue();

	public void threePointsBlue();

	public void onePointRed();

	public void threePointsRed();

	public void incrementWarningBlue();

	public void incrementWarningRed();

	public String isOver();

	public void blueScoredKo();

	public void redScoredKo();

	public String getIsOverRes();

	public void reset();

	public void setBlueAthleteSurname(String surname);

	public void setRedAthleteSurname(String surname);

	public void setDeltaOne(int delta);

	public void setDeltaThree(int delta);

	public Integer getDeltaOne();

	public Integer getDeltaThree();

	public void setMaxRound(int max);

	public int cancelLastOperation();

	/**
	 * 
	 * @param punti
	 * @return paths of the JPG blue-score
	 */
	public String[] updateBlueScore();

	/**
	 * 
	 * @param punti
	 * @return paths of the JPG redore
	 */
	public String[] updateRedScore();

	/**
	 * 
	 * @param warnings
	 * @return paths of the JPG red-warning
	 */
	public String updateRedWarning();

	/**
	 * 
	 * @param warnings
	 * @return paths of the JPG blue-warning
	 */
	public String updateBlueWarning();

	/**
	 * Create a Clip object and play it
	 * 
	 */
	public void playBeepOne();

	/**
	 * Create a Clip object and play it
	 * 
	 */
	public void playBeepTwo();

	/**
	 * Insert in a List<Match> the match concluded
	 * 
	 * @param atleta1
	 * @param atleta2
	 * @param risultato
	 */
	public void insertListMatch(String atleta1, String atleta2, String risultato);

	/**
	 * Set the matches list
	 * 
	 * @param lista
	 */
	public void setListMatch(ArrayList<Match> lista);

	/**
	 * Get the matches list
	 * 
	 * @return
	 */
	public ArrayList<Match> getListMatch();

	/**
	 * Adds the fights fought in the file(data backup)
	 */
	public void insertListMatchFile();

	/**
	 * 
	 * @return Get the fights saved in the file .dat
	 */
	public ArrayList<Match> getListMatchFile();
}
