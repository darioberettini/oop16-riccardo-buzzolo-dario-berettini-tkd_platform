package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class FightImpl implements Fight, Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Match> listMatch;
	private Integer seconds = 60;
	private Integer secondsPause = 30;
	private Integer round = 1;
	private int bluePoints = 0;
	private int redPoints = 0;
	private int blueWarnings = 0;
	private int redWarnings = 0;
	private boolean koBlue = false;
	private boolean koRed = false;
	private String overResult = "not over";
	private String surnameBlueAthlete;
	private String surnameRedAthlete;
	private int modSeconds = 60;
	private int modSecondsPause = 30;
	private int deltaOne = 1;
	private int deltaThree = 3;
	private int maxRound = 3;

	private List<Integer> operationDoneList = new ArrayList<>();

	public FightImpl() {
		this.listMatch = getListMatchFile();
	}

	// Seconds handling
	public int getModFightSeconds() {
		return this.modSeconds;
	}

	public int getModPauseSeconds() {
		return this.modSecondsPause;
	}

	public void setModFightSeconds(int value) {
		this.modSeconds = value;
	}

	public void setModFightPauseSeconds(int value) {
		this.modSecondsPause = value;
	}

	public Integer getSeconds() {
		return seconds;
	}

	public void setSeconds(int newValue) {
		this.seconds = newValue;
	}

	public void setSecondsPause(int secondsPause) {
		this.secondsPause = secondsPause;
	}

	public void decrementSeconds() {
		this.seconds--;
	}

	public Integer getSecondsPause() {
		return secondsPause;
	}

	public void incrementSecondsPause() {
		this.secondsPause++;
	}

	public void decrementSecondsPause() {
		this.secondsPause--;
	}

	// Round handling
	public void incrementRound() {
		this.round++;
	}

	public Integer getRound() {
		return this.round;
	}

	public Integer getMaxround() {
		return this.maxRound;
	}

	public void setMaxRound(int max) {
		this.maxRound = max;
	}

	// Points handling
	public void setDeltaOne(int delta) {
		this.deltaOne = delta;
	}

	public void setDeltaThree(int delta) {
		this.deltaThree = delta;
	}

	public Integer getDeltaOne() {
		return this.deltaOne;
	}

	public Integer getDeltaThree() {
		return this.deltaThree;
	}

	public void onePointBlue() {
		this.bluePoints = this.bluePoints + this.deltaOne;
		operationDoneList.add(0);
	}

	public void threePointsBlue() {
		this.bluePoints = this.bluePoints + this.deltaThree;
		operationDoneList.add(2);

	}

	public void onePointRed() {
		this.redPoints = this.redPoints + this.deltaOne;
		operationDoneList.add(1);

	}

	public void threePointsRed() {
		this.redPoints = this.redPoints + this.deltaThree;
		operationDoneList.add(3);

	}

	public int getBluePoints() {
		return this.bluePoints;
	}

	public int getRedPoints() {
		return this.redPoints;
	}

	// Warning handling
	public void incrementWarningBlue() {
		if (this.blueWarnings < 10)
			this.blueWarnings++;
		operationDoneList.add(4);
	}

	public void incrementWarningRed() {
		if (this.redWarnings < 10)
			this.redWarnings++;
		operationDoneList.add(5);
	}

	public int getBlueWarning() {
		return this.blueWarnings;
	}

	public int getRedWarning() {
		return this.redWarnings;
	}

	// Surname handling
	public void setBlueAthleteSurname(String surname) {
		this.surnameBlueAthlete = surname;
	}

	public void setRedAthleteSurname(String surname) {
		this.surnameRedAthlete = surname;
	}

	// K.O. handling
	public void blueScoredKo() {
		this.koBlue = true;
	}

	public void redScoredKo() {
		this.koRed = true;
	}

	public String[] updateBlueScore() {

		String[] s = new String[2];
		if (bluePoints < 10) {

			s[0] = "/puntitaekwondo/" + bluePoints + "_blue.png";
			s[1] = "/puntitaekwondo/" + 0 + "_blue.png";
			return s;

		}
		if (bluePoints >= 10) {

			for (int i = 10; i < 100; i += 10) {
				if (bluePoints - i <= 9) {

					for (int j = 0; j < 10; j++) {

						if (bluePoints - i - j == 0) {
							s[1] = "/puntitaekwondo/" + i / 10 + "_blue.png";
							s[0] = "/puntitaekwondo/" + j + "_blue.png";
							return s;
						}
					}
				}
			}
		}
		return null;
	}

	public String[] updateRedScore() {

		String[] s = new String[2];
		if (redPoints < 10) {
			s[0] = "/puntitaekwondo/" + redPoints + "_red.png";
			s[1] = "/puntitaekwondo/" + 0 + "_red.png";
			return s;
		}
		if (redPoints >= 10) {

			for (int i = 10; i < 50; i += 10) {

				if (redPoints - i <= 9) {

					for (int j = 0; j < 10; j++) {

						if (redPoints - i - j == 0) {
							s[1] = "/puntitaekwondo/" + i / 10 + "_red.png";
							s[0] = "/puntitaekwondo/" + j + "_red.png";
							return s;
						}
					}
				}
			}
		}
		return null;
	}

	public String updateRedWarning() {
		if (redWarnings % 2 == 0) {
			return "/puntitaekwondo/red_warning.png";
		} else {
			return "/puntitaekwondo/yellow_warning.png";
		}

	}

	public String updateBlueWarning() {

		if (blueWarnings % 2 == 0) {
			return "/puntitaekwondo/red_warning.png";
		} else {
			return "/puntitaekwondo/yellow_warning.png";
		}

	}

	public void playBeepOne() {
		try {
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(FightImpl.class.getResource("/audio/beep1.wav")));
			clip.start();
			Thread.sleep(clip.getMicrosecondLength() / 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void playBeepTwo() {

		try {
			Clip clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(FightImpl.class.getResource("/audio/beep2.wav")));
			clip.start();
			Thread.sleep(clip.getMicrosecondLength() / 1000);
		} catch (Exception e) {
		}
	}

	public void insertListMatch(String firstAthleteSurname, String secondAthleteSurname, String result) {

		this.listMatch.add(new MatchImpl(firstAthleteSurname, secondAthleteSurname, result));
	}

	public void setListMatch(ArrayList<Match> lista) {

		this.listMatch = lista;
	}

	public ArrayList<Match> getListMatch() {

		return this.listMatch;
	}

	public void insertListMatchFile() {

		String homeDir = System.getProperty("user.home");
		new File(homeDir + "\\storici").mkdir();

		try {

			FileOutputStream stream = new FileOutputStream(homeDir + "\\storici\\ListaMatch.dat");

			ObjectOutputStream osStream = new ObjectOutputStream(stream);

			osStream.writeObject(listMatch);

			osStream.flush();

			osStream.close();

		} catch (Exception e) {

			System.out.println("I/O errore" + e);
		}
	}

	public ArrayList<Match> getListMatchFile() {
		String homeDir = System.getProperty("user.home");
		new File(homeDir + "\\storici").mkdir();
		try {
			FileInputStream stream = new FileInputStream(homeDir + "\\storici\\ListaMatch.dat");

			ObjectInputStream osStream = new ObjectInputStream(stream);

			@SuppressWarnings("unchecked")
			ArrayList<Match> listaMatchFile = (ArrayList<Match>) osStream.readObject();

			osStream.close();

			return listaMatchFile;

		} catch (Exception e) {

			System.out.println("I/O errore di stampa" + e);
		}

		ArrayList<Match> arrayEmpty = new ArrayList<Match>();
		return arrayEmpty;
	}

	public String getIsOverRes() {
		return overResult;
	}

	public String isOver() {
		if (this.getRound() >= this.maxRound || (this.getRound() == this.maxRound - 1 && this.getSeconds() == 0)) {
			if (this.getBluePoints() >= this.getRedPoints() + 12) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete, this.surnameBlueAthlete
						+ " vince per differenza di punti. " + this.bluePoints + "-" + this.redPoints);
				overResult = this.surnameBlueAthlete + " vince per differenza di punti. " + this.bluePoints + "-"
						+ this.redPoints;
				insertListMatchFile();
				return this.surnameBlueAthlete + " vince per differenza di punti. " + this.bluePoints + "-"
						+ this.redPoints;
			}
			if (this.getRedPoints() >= this.getBluePoints() + 12) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete,
						this.surnameRedAthlete + " vince per differenza di punti. " + this.redPoints + "-" + this.bluePoints);
				overResult = this.surnameRedAthlete + " vince per differenza di punti. " + this.redPoints + "-"
						+ this.bluePoints;
				insertListMatchFile();
				return this.surnameRedAthlete + " vince per differenza di punti. " + this.redPoints + "-" + this.bluePoints;
			}
		}
		if (this.getRound() == this.maxRound && this.seconds == 0) {
			if (this.getBluePoints() > this.getRedPoints()) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete,
						this.surnameBlueAthlete + " vince. Punteggio:" + this.bluePoints + "-" + this.redPoints);
				overResult = this.surnameBlueAthlete + " vince. Punteggio:" + this.bluePoints + "-" + this.redPoints;
				insertListMatchFile();
				return this.surnameBlueAthlete + " vince. Punteggio:" + this.bluePoints + "-" + this.redPoints;
			} else if (this.getBluePoints() < this.getRedPoints()) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete,
						this.surnameRedAthlete + " vince. Punteggio:" + this.redPoints + "-" + this.bluePoints);
				overResult = this.surnameRedAthlete + " vince. Punteggio:" + this.redPoints + "-" + this.bluePoints;
				insertListMatchFile();
				return this.surnameRedAthlete + " vince. Punteggio:" + this.redPoints + "-" + this.bluePoints;
			} else if (this.getBluePoints() == this.getRedPoints()) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete,
						"Pareggio. Punteggio:" + this.redPoints + "-" + this.bluePoints);
				overResult = "Pareggio. Punteggio:" + this.redPoints + "-" + this.bluePoints;
				insertListMatchFile();
				return "Pareggio. Punteggio:" + this.redPoints + "-" + this.bluePoints;
			}
		} else {

			if (this.getBlueWarning() == 10) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete,
						this.surnameRedAthlete + " vince. " + this.surnameBlueAthlete + " ha raggiunto 10 ammonizioni.");
				overResult = this.surnameRedAthlete + " vince. " + this.surnameBlueAthlete
						+ " ha raggiunto 10 ammonizioni.";
				insertListMatchFile();
				return this.surnameRedAthlete + " vince. " + this.surnameBlueAthlete + " ha raggiunto 10 ammonizioni.";
			}
			if (this.getRedWarning() == 10) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete,
						this.surnameBlueAthlete + " vince. " + this.surnameRedAthlete + " ha raggiunto 10 ammonizioni.");
				overResult = this.surnameBlueAthlete + " vince. " + this.surnameRedAthlete
						+ " ha raggiunto 10 ammonizioni.";
				insertListMatchFile();
				return this.surnameBlueAthlete + " vince. " + this.surnameRedAthlete + " ha raggiunto 10 ammonizioni.";
			}
			if (this.koBlue) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete, this.surnameBlueAthlete + " vince per ko.");
				overResult = this.surnameBlueAthlete + " vince per ko.";
				insertListMatchFile();
				return this.surnameBlueAthlete + " vince per ko.";
			}
			if (this.koRed) {
				insertListMatch(surnameBlueAthlete, surnameRedAthlete, this.surnameRedAthlete + " vince per ko.");
				overResult = this.surnameRedAthlete + " vince per ko.";
				insertListMatchFile();
				return this.surnameRedAthlete + " vince per ko.";
			}
		}
		return "not over";

	}

	public int cancelLastOperation() {
		if (this.operationDoneList.isEmpty() == false) {
			if (operationDoneList.get(operationDoneList.size() - 1) == 0) {
				bluePoints = bluePoints - deltaOne;
				operationDoneList.remove(operationDoneList.size() - 1);
				return 0;
			}
			if (operationDoneList.get(operationDoneList.size() - 1) == 1) {
				redPoints = redPoints - deltaOne;
				operationDoneList.remove(operationDoneList.size() - 1);
				return 1;
			}
			if (operationDoneList.get(operationDoneList.size() - 1) == 2) {
				bluePoints = bluePoints - deltaThree;
				operationDoneList.remove(operationDoneList.size() - 1);
				return 2;
			}
			if (operationDoneList.get(operationDoneList.size() - 1) == 3) {
				redPoints = redPoints - deltaThree;
				operationDoneList.remove(operationDoneList.size() - 1);
				return 3;
			}
			if (operationDoneList.get(operationDoneList.size() - 1) == 4) {
				blueWarnings--;
				operationDoneList.remove(operationDoneList.size() - 1);
				return 4;
			}
			if (operationDoneList.get(operationDoneList.size() - 1) == 5) {
				redWarnings--;
				operationDoneList.remove(operationDoneList.size() - 1);
				return 5;
			}
		}
		return -1;
	}

	public void reset() {
		seconds = this.modSeconds;
		secondsPause = this.modSecondsPause;
		round = 1;
		bluePoints = 0;
		redPoints = 0;
		blueWarnings = 0;
		redWarnings = 0;
		koBlue = false;
		koRed = false;
		overResult = "not over";
	}

}
