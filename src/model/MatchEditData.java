package model;

public class MatchEditData {

	private int timerValue;
	private int pauseValue;
	private int roundValue;
	private int onePointValue;
	private int threePointValue;

	public MatchEditData(int timer, int pause, int round, int onePointMod, int threePointMod) {

		this.timerValue = timer;
		this.pauseValue = pause;
		this.roundValue = round;
		this.onePointValue = onePointMod;
		this.threePointValue = threePointMod;

	}

	public int getTimerValue() {
		return this.timerValue;
	}

	public int getPauseValue() {
		return this.pauseValue;
	}

	public int getRoundValue() {
		return this.roundValue;
	}

	public int getOnePointValue() {
		return this.onePointValue;
	}

	public int getThreePointsValue() {
		return this.threePointValue;
	}

}
