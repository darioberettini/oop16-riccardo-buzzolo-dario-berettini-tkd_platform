package model;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FormImpl implements Form {

	private List<Athlete> listAthletes = new ArrayList<>();

	public FormImpl() {

		this.listAthletes = getListAthletesFormFile();
	}

	public String[] getScoreRed(int points) {

		String[] s = new String[3];

		if (points < 100) {

			int unit = points - ((points / 10) * 10);
			int dozen = points - unit;
			s[0] = "/puntitaekwondo/" + 0 + "_red.png";
			s[1] = "/puntitaekwondo/" + dozen / 10 + "_red.png";
			s[2] = "/puntitaekwondo/" + unit + "_red.png";
			return s;
		}

		return null;
	}

	public List<Athlete> getListAthletesForm() {

		return listAthletes;
	}

	public ArrayList<String> printAthletes() {

		ArrayList<String> printedList = new ArrayList<>();
		for (Athlete athlete : listAthletes) {

			printedList.add(athlete.getName() + " " + athlete.getSurname());
		}

		return printedList;
	}

	public void addAhtletesForm(Athlete athlete) {

		this.listAthletes.add(athlete);
	}

	public void addListAthletesFormFile() {

		String homeDir = System.getProperty("user.home");
		new File(homeDir + "\\storici").mkdir();

		try {

			FileOutputStream stream = new FileOutputStream(homeDir + "\\storici\\ListaAtletiForma.dat");
			ObjectOutputStream osStream = new ObjectOutputStream(stream);

			osStream.writeObject(listAthletes);
			osStream.flush();
			osStream.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public ArrayList<Athlete> getListAthletesFormFile() {

		String homeDir = System.getProperty("user.home");
		new File(homeDir + "\\storici").mkdir();
		try {
			FileInputStream stream = new FileInputStream(homeDir + "\\storici\\ListaAtletiForma.dat");

			ObjectInputStream osStream = new ObjectInputStream(stream);

			@SuppressWarnings("unchecked")
			ArrayList<Athlete> listAthletes = (ArrayList<Athlete>) osStream.readObject();

			osStream.close();

			return listAthletes;

		} catch (Exception e) {

			e.printStackTrace();
		}

		ArrayList<Athlete> arrayEmpty = new ArrayList<Athlete>();

		return arrayEmpty;
	}
}
