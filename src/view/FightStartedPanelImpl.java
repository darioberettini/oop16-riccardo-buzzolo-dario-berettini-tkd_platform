package view;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import controller.FightController;
import model.MatchEditData;

public class FightStartedPanelImpl extends JFrame implements FightStartedPanel {

	// EDIT COMPONENTS
	// private MatchEditData data;
	private final JTextField secondsFightTextEdit = new JTextField("");
	private final JTextField secondsPauseTextEdit = new JTextField("");
	private final JTextField roundLabelEdit = new JTextField("");
	private final JTextField onePointRedButtonEdit = new JTextField("");
	private final JTextField threePointRedButtonEdit = new JTextField("");

	private static final long serialVersionUID = 1L;
	private FightController fightController;
	private JPanel mainPanel = new JPanel();
	private final JButton startButton = new JButton("Start");
	private final JButton pauseButton = new JButton("Pause");
	private final JLabel instructionsLabel = new JLabel("?");
	private final JButton undoButton = new JButton("Undo");
	private final JButton editButton = new JButton("Edit");
	private final JButton onePointBlueButton = new JButton("+1");
	private final JButton onePointRedButton = new JButton("+1");
	private final JButton threePointBlueButton = new JButton("+3");
	private final JButton threePointRedButton = new JButton("+3");
	private final JButton warningOneRedButton = new JButton("X");
	private final JLabel[] warningRed = new JLabel[10];
	private final JButton warningOneBlueButton = new JButton("X");
	private final JButton koBlueButton = new JButton("KO");
	private final JButton koRedButton = new JButton("KO");
	private final JLabel[] warningBlue = new JLabel[10];
	private final JLabel secondsFightText = new JLabel("01:00");
	private final JLabel breakLabel = new JLabel("Break: ");
	private final JLabel secondsPauseText = new JLabel("00:30");
	private final JLabel roundLabel = new JLabel("    R - 1");
	private final JLabel unitBluePointsImage = new JLabel(
			new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_blue.png")));
	private final JLabel dozenBluePointsImage = new JLabel(
			new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_blue.png")));
	private final JLabel dozenRedPointsImage = new JLabel(
			new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
	private final JLabel unitRedPointsImage = new JLabel(
			new ImageIcon(FightStartedPanelImpl.class.getResource("/puntitaekwondo/0_red.png")));
	private final JLabel surnameFirstAthleteLabel = new JLabel();
	private final JLabel surnameSecondAthleteLabel = new JLabel();
	private String surnameFirstAthlete;
	private String surnameSecondAthlete;
	private int xCoordinateBlue = 1125;
	private int xCoordinateRed = 10;

	private KeyEventDispatcher keyEventDispatcher;

	public FightStartedPanelImpl(final String cognome1, final String cognome2, final boolean adminLog) {

		this.surnameFirstAthlete = cognome1;
		this.surnameSecondAthlete = cognome2;
		this.surnameFirstAthleteLabel.setText("                        " + this.surnameFirstAthlete);
		this.surnameSecondAthleteLabel.setText("                     " + this.surnameSecondAthlete);
		this.setVisible(true);
		this.setResizable(false);
		this.setBounds(100, 100, 1200, 700);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);
		mainPanel.setBackground(new Color(0, 0, 0));

		startButton.setForeground(new Color(0, 0, 0));
		startButton.setFont(new Font("Arial", Font.BOLD, 20));
		startButton.setBounds(10, 10, 150, 50);
		mainPanel.add(startButton);

		pauseButton.setForeground(new Color(0, 0, 0));
		pauseButton.setFont(new Font("Arial", Font.BOLD, 20));
		pauseButton.setBounds(163, 10, 150, 50);
		mainPanel.add(pauseButton);

		instructionsLabel.setForeground(new Color(255, 255, 255));
		instructionsLabel.setFont(new Font("Arial", Font.BOLD, 20));
		instructionsLabel.setBounds(330, 10, 20, 50);
		instructionsLabel.setOpaque(true);
		instructionsLabel.setBackground(new Color(0, 0, 0));
		mainPanel.add(instructionsLabel);

		roundLabel.setFont(new Font("Arial", Font.BOLD, 20));
		roundLabel.setBackground(new Color(255, 165, 0));
		roundLabel.setForeground(new Color(0, 0, 0));
		roundLabel.setOpaque(true);
		roundLabel.setBounds(545, 520, 100, 100);
		mainPanel.add(roundLabel);

		undoButton.setForeground(new Color(0, 0, 0));
		undoButton.setFont(new Font("Arial", Font.BOLD, 20));
		undoButton.setBounds(1035, 10, 150, 50);
		mainPanel.add(undoButton);

		if (adminLog) {
			editButton.setForeground(new Color(0, 0, 0));
			editButton.setFont(new Font("Arial", Font.BOLD, 20));
			editButton.setBounds(883, 10, 150, 50);
			mainPanel.add(editButton);
		}

		surnameFirstAthleteLabel.setForeground(new Color(255, 255, 255));
		surnameFirstAthleteLabel.setFont(new Font("Arial", Font.BOLD, 30));
		surnameFirstAthleteLabel.setBounds(10, 65, 500, 40);
		surnameFirstAthleteLabel.setOpaque(true);
		surnameFirstAthleteLabel.setBackground(new Color(255, 0, 0));
		mainPanel.add(surnameFirstAthleteLabel);

		surnameSecondAthleteLabel.setForeground(new Color(255, 255, 255));
		surnameSecondAthleteLabel.setFont(new Font("Arial", Font.BOLD, 30));
		surnameSecondAthleteLabel.setBounds(700, 65, 485, 40);
		surnameSecondAthleteLabel.setOpaque(true);
		surnameSecondAthleteLabel.setBackground(new Color(0, 0, 255));
		mainPanel.add(surnameSecondAthleteLabel);

		secondsFightText.setFont(new Font("Arial", Font.BOLD, 50));
		secondsFightText.setForeground(new Color(255, 255, 255));
		secondsFightText.setBounds(530, 10, 150, 50);
		mainPanel.add(secondsFightText);

		breakLabel.setFont(new Font("Arial", Font.BOLD, 30));
		breakLabel.setForeground(new Color(255, 255, 255));
		breakLabel.setBounds(515, 60, 200, 50);
		mainPanel.add(breakLabel);

		secondsPauseText.setFont(new Font("Arial", Font.BOLD, 30));
		secondsPauseText.setForeground(new Color(255, 255, 255));
		secondsPauseText.setBounds(615, 60, 200, 50);
		mainPanel.add(secondsPauseText);

		unitRedPointsImage.setBounds(390, 100, 200, 420);
		mainPanel.add(unitRedPointsImage);
		dozenRedPointsImage.setBounds(190, 100, 200, 420);
		mainPanel.add(dozenRedPointsImage);

		unitBluePointsImage.setBounds(800, 100, 200, 420);
		mainPanel.add(unitBluePointsImage);
		dozenBluePointsImage.setBounds(600, 100, 200, 420);
		mainPanel.add(dozenBluePointsImage);

		koRedButton.setForeground(new Color(0, 0, 0));
		koRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		koRedButton.setBounds(90, 520, 100, 60);
		mainPanel.add(koRedButton);

		onePointRedButton.setForeground(new Color(0, 0, 0));
		onePointRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		onePointRedButton.setBounds(190, 520, 100, 60);
		mainPanel.add(onePointRedButton);

		threePointRedButton.setForeground(new Color(0, 0, 0));
		threePointRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		threePointRedButton.setBounds(290, 520, 100, 60);
		mainPanel.add(threePointRedButton);

		warningOneBlueButton.setForeground(new Color(0, 0, 0));
		warningOneBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		warningOneBlueButton.setBounds(700, 520, 100, 60);
		mainPanel.add(warningOneBlueButton);

		threePointBlueButton.setForeground(new Color(0, 0, 0));
		threePointBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		threePointBlueButton.setBounds(800, 520, 100, 60);
		mainPanel.add(threePointBlueButton);

		onePointBlueButton.setForeground(new Color(0, 0, 0));
		onePointBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		onePointBlueButton.setBounds(900, 520, 100, 60);
		mainPanel.add(onePointBlueButton);

		koBlueButton.setForeground(new Color(0, 0, 0));
		koBlueButton.setFont(new Font("Arial", Font.BOLD, 20));
		koBlueButton.setBounds(1000, 520, 100, 60);
		mainPanel.add(koBlueButton);

		warningOneRedButton.setForeground(new Color(0, 0, 0));
		warningOneRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		warningOneRedButton.setBounds(390, 520, 100, 60);
		mainPanel.add(warningOneRedButton);

		this.pauseButton.setEnabled(false);
		this.onePointBlueButton.setEnabled(false);
		this.onePointRedButton.setEnabled(false);
		this.threePointBlueButton.setEnabled(false);
		this.threePointRedButton.setEnabled(false);
		this.koBlueButton.setEnabled(false);
		this.koRedButton.setEnabled(false);
		this.warningOneBlueButton.setEnabled(false);
		this.warningOneRedButton.setEnabled(false);
		this.instructionsLabel.addMouseListener(new LabelAdapter());

		startButton.addActionListener(e -> {

			fightController.startTimerFight();

		});

		pauseButton.addActionListener(e -> {

			fightController.stopTimerFight();
			startButton.setEnabled(true);

		});

		onePointBlueButton.addActionListener(e -> {

			fightController.blueScoredOne();

		});

		onePointRedButton.addActionListener(e -> {

			fightController.redScoredOne();

		});

		threePointBlueButton.addActionListener(e -> {

			fightController.blueScoredThree();
		});

		threePointRedButton.addActionListener(e -> {

			fightController.redScoredThree();

		});

		warningOneBlueButton.addActionListener(e -> {

			fightController.updateBlueWarning();
		});

		warningOneRedButton.addActionListener(e -> {

			fightController.updateRedWarning();

		});

		koBlueButton.addActionListener(e -> {

			fightController.blueScoredKo();

		});

		koRedButton.addActionListener(e -> {
			fightController.redScoredKo();

		});

		undoButton.addActionListener(e -> {

			fightController.cancelOperation();
		});

		editButton.addActionListener(e -> {

			if (editButton.getText().equals("Edit")) {
				startButton.setEnabled(false);
				undoButton.setEnabled(false);
				editButton.setText("Save");
				KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
				showEditUI();
				SwingUtilities.updateComponentTreeUI(this);
			} else if (editButton.getText().equals("Save")) {

				if (!secondsFightTextEdit.getText().matches("[0-9]+") || !secondsPauseTextEdit.getText().matches("[0-9]+")
						|| !roundLabelEdit.getText().matches("[0-9]+") || !onePointRedButtonEdit.getText().matches("[0-9]+")
						|| !threePointRedButtonEdit.getText().matches("[0-9]+")) {
					JFrame frame = new JFrame("Errore");
					JOptionPane.showMessageDialog(frame, "Inserire solo numeri.");
				} else {
					editButton.setText("Edit");
					startButton.setEnabled(true);
					undoButton.setEnabled(false);
					KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
					hideEditUI();
					fightController.editParameters(new MatchEditData(Integer.parseInt(secondsFightTextEdit.getText()),
							Integer.parseInt(secondsPauseTextEdit.getText()), Integer.parseInt(roundLabelEdit.getText()),
							Integer.parseInt(onePointRedButtonEdit.getText()),
							Integer.parseInt(threePointRedButtonEdit.getText())));
					SwingUtilities.updateComponentTreeUI(this);
				}
			}
		});

		this.keyEventDispatcher = new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				if (e.getID() == KeyEvent.KEY_TYPED) {
					String key = "" + e.getKeyChar();

					if (key.equalsIgnoreCase("T")) {
						startButton.doClick();
					} else if (key.equalsIgnoreCase("Y")) {
						pauseButton.doClick();
					} else if (key.equalsIgnoreCase("Q")) {
						warningOneRedButton.doClick();
					} else if (key.equalsIgnoreCase("A")) {
						onePointRedButton.doClick();
					} else if (key.equalsIgnoreCase("Z")) {
						threePointRedButton.doClick();
					} else if (key.equalsIgnoreCase("O")) {
						warningOneBlueButton.doClick();
					} else if (key.equalsIgnoreCase("K")) {
						onePointBlueButton.doClick();
					} else if (key.equalsIgnoreCase("M")) {
						threePointBlueButton.doClick();
					}

				}
				return rootPaneCheckingEnabled;
			}
		};

		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent we) {
				String ObjButtons[] = { "Si", "No" };
				int PromptResult = JOptionPane.showOptionDialog(null,
						"Lo stato del combattimento verrà perso, sei sicuro di vole uscire?", "Attenzione",
						JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, ObjButtons, ObjButtons[1]);
				if (PromptResult == 0) {
					KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
					dispose();
					fightController.endFight(true);
				}
			}
		});
	}

	// SETTER PER PARAMETRI
	public void setSecondsFightText(String newValue) {
		this.secondsFightText.setText(newValue);
	}

	public void setSecondsPauseText(String newValue) {
		this.secondsPauseText.setText(newValue);
	}

	public void setRoundText(Integer newValue) {
		this.roundLabel.setText("    R - " + newValue.toString());
	}

	public void setOnePointButtonLabel(String newValue) {
		this.onePointBlueButton.setText(newValue);
		this.onePointRedButton.setText(newValue);
	}

	public void setThreePointButtonLabel(String newValue) {
		this.threePointBlueButton.setText(newValue);
		this.threePointRedButton.setText(newValue);

	}

	public void setBluePoint(URL unit, URL dozen) {
		dozenBluePointsImage.setIcon(new ImageIcon(dozen));
		unitBluePointsImage.setIcon(new ImageIcon(unit));
	}

	public void setRedPoint(URL unit, URL dozen) {
		dozenRedPointsImage.setIcon(new ImageIcon(dozen));
		unitRedPointsImage.setIcon(new ImageIcon(unit));
	}

	public void setBlueWarnings(URL warning, int index) {

		if (index % 2 == 0) {

			mainPanel.remove(warningBlue[index - 1]);
			warningBlue[index - 1] = new JLabel(new ImageIcon(warning));
			warningBlue[index - 1].setBounds(xCoordinateBlue, 570, 60, 110);
			mainPanel.add(warningBlue[index - 1]);
			xCoordinateBlue = xCoordinateBlue - 63;
		} else {
			warningBlue[index] = new JLabel(new ImageIcon(warning));
			mainPanel.add(warningBlue[index]);
			warningBlue[index].setBounds(xCoordinateBlue, 570, 60, 110);
		}
		SwingUtilities.updateComponentTreeUI(this);
		// operationDoneList.add(5);
	}

	public void setRedWarnings(URL warning, int index) {

		if (index % 2 == 0) {
			mainPanel.remove(warningRed[index - 1]);
			warningRed[index - 1] = new JLabel(new ImageIcon(warning));
			warningRed[index - 1].setBounds(xCoordinateRed, 570, 60, 110);
			mainPanel.add(warningRed[index - 1]);
			xCoordinateRed = xCoordinateRed + 63;
		} else {
			warningRed[index] = new JLabel(new ImageIcon(warning));
			mainPanel.add(warningRed[index]);
			warningRed[index].setBounds(xCoordinateRed, 570, 60, 110);
		}

		SwingUtilities.updateComponentTreeUI(this);
		// operationDoneList.add(5);
	}

	public void updateButtonState(int state) {
		if (state == 1) {
			startButton.setText("Start");
			pauseButton.setEnabled(false);
			warningOneBlueButton.setEnabled(false);
			warningOneRedButton.setEnabled(false);
			onePointBlueButton.setEnabled(false);
			onePointRedButton.setEnabled(false);
			threePointBlueButton.setEnabled(false);
			threePointRedButton.setEnabled(false);
			koBlueButton.setEnabled(false);
			koRedButton.setEnabled(false);
		}
		if (state == 2) {
			editButton.setEnabled(false);
			undoButton.setEnabled(true);
			startButton.setText("Resume");
			startButton.setEnabled(false);
			pauseButton.setEnabled(true);
			warningOneBlueButton.setEnabled(true);
			warningOneRedButton.setEnabled(true);
			onePointBlueButton.setEnabled(true);
			onePointRedButton.setEnabled(true);
			threePointBlueButton.setEnabled(true);
			threePointRedButton.setEnabled(true);
			koBlueButton.setEnabled(true);
			koRedButton.setEnabled(true);
		}
		if (state == 3) {
			startButton.setText("Resume");
			startButton.setEnabled(true);
			pauseButton.setEnabled(false);
			warningOneBlueButton.setEnabled(true);
			warningOneRedButton.setEnabled(true);
			onePointBlueButton.setEnabled(true);
			onePointRedButton.setEnabled(true);
			threePointBlueButton.setEnabled(true);
			threePointRedButton.setEnabled(true);
			koBlueButton.setEnabled(true);
			koRedButton.setEnabled(true);
		}
		if (state == 4) {
			startButton.setText("Start");
			startButton.setEnabled(false);
			pauseButton.setEnabled(false);
			warningOneBlueButton.setEnabled(true);
			warningOneRedButton.setEnabled(true);
			onePointBlueButton.setEnabled(true);
			onePointRedButton.setEnabled(true);
			threePointBlueButton.setEnabled(true);
			threePointRedButton.setEnabled(true);
			koBlueButton.setEnabled(true);
			koRedButton.setEnabled(true);
		}
		if (state == 5) {
			startButton.setText("Start");
			startButton.setEnabled(true);
			pauseButton.setEnabled(false);
			warningOneBlueButton.setEnabled(true);
			warningOneRedButton.setEnabled(true);
			onePointBlueButton.setEnabled(true);
			onePointRedButton.setEnabled(true);
			threePointBlueButton.setEnabled(true);
			threePointRedButton.setEnabled(true);
			koBlueButton.setEnabled(true);
			koRedButton.setEnabled(true);
		}
		if (state == 6) {
			startButton.setText("Start");
			startButton.setEnabled(true);
			pauseButton.setEnabled(false);
			warningOneBlueButton.setEnabled(false);
			warningOneRedButton.setEnabled(false);
			onePointBlueButton.setEnabled(false);
			onePointRedButton.setEnabled(false);
			threePointBlueButton.setEnabled(false);
			threePointRedButton.setEnabled(false);
			koBlueButton.setEnabled(false);
			koRedButton.setEnabled(false);
		}
	}

	public void removeWarning(int color, int index, int coordinateMoved) {
		if (color == 0) {
			mainPanel.remove(warningBlue[index + 1]);
			xCoordinateBlue = xCoordinateBlue + coordinateMoved;
		} else if (color == 1) {
			mainPanel.remove(warningRed[index + 1]);
			xCoordinateRed = xCoordinateRed - coordinateMoved;
		}
		SwingUtilities.updateComponentTreeUI(this);
	}

	public void endFightMessage(String message) {

		JFrame frame = new JFrame();
		JOptionPane.showMessageDialog(frame, message);
		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);

		this.dispose();
	}

	public void showEditUI() {
		mainPanel.remove(secondsFightText);
		secondsFightTextEdit.setFont(new Font("Arial", Font.BOLD, 50));
		secondsFightTextEdit.setBackground(new Color(0, 0, 0));
		secondsFightTextEdit.setForeground(new Color(255, 255, 255));
		secondsFightTextEdit.setBounds(530, 10, 150, 50);
		mainPanel.add(secondsFightTextEdit);

		mainPanel.remove(secondsPauseText);
		secondsPauseTextEdit.setFont(new Font("Arial", Font.BOLD, 30));
		secondsPauseTextEdit.setBackground(new Color(0, 0, 0));
		secondsPauseTextEdit.setForeground(new Color(255, 255, 255));
		secondsPauseTextEdit.setBounds(615, 60, 200, 50);
		mainPanel.add(secondsPauseTextEdit);

		mainPanel.remove(roundLabel);
		roundLabelEdit.setFont(new Font("Arial", Font.BOLD, 20));
		roundLabelEdit.setBackground(new Color(255, 165, 0));
		roundLabelEdit.setForeground(new Color(0, 0, 0));
		roundLabelEdit.setOpaque(true);
		roundLabelEdit.setBounds(545, 520, 100, 100);
		mainPanel.add(roundLabelEdit);

		mainPanel.remove(onePointRedButton);
		onePointRedButtonEdit.setForeground(new Color(0, 0, 0));
		onePointRedButtonEdit.setFont(new Font("Arial", Font.BOLD, 20));
		onePointRedButtonEdit.setBounds(190, 520, 100, 60);
		mainPanel.add(onePointRedButtonEdit);

		mainPanel.remove(threePointRedButton);
		threePointRedButtonEdit.setForeground(new Color(0, 0, 0));
		threePointRedButtonEdit.setFont(new Font("Arial", Font.BOLD, 20));
		threePointRedButtonEdit.setBounds(290, 520, 100, 60);
		mainPanel.add(threePointRedButtonEdit);
	}

	public void hideEditUI() {
		mainPanel.remove(secondsFightTextEdit);
		secondsFightText.setFont(new Font("Arial", Font.BOLD, 50));
		secondsFightText.setForeground(new Color(255, 255, 255));
		secondsFightText.setBounds(530, 10, 150, 50);
		mainPanel.add(secondsFightText);

		mainPanel.remove(secondsPauseTextEdit);
		secondsPauseText.setFont(new Font("Arial", Font.BOLD, 30));
		secondsPauseText.setForeground(new Color(255, 255, 255));
		secondsPauseText.setBounds(615, 60, 200, 50);
		mainPanel.add(secondsPauseText);

		mainPanel.remove(roundLabelEdit);
		roundLabel.setFont(new Font("Arial", Font.BOLD, 20));
		roundLabel.setBackground(new Color(255, 165, 0));
		roundLabel.setForeground(new Color(0, 0, 0));
		roundLabel.setOpaque(true);
		roundLabel.setBounds(545, 520, 100, 100);
		mainPanel.add(roundLabel);
		;

		mainPanel.remove(onePointRedButtonEdit);
		onePointRedButton.setForeground(new Color(0, 0, 0));
		onePointRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		onePointRedButton.setBounds(190, 520, 100, 60);
		mainPanel.add(onePointRedButton);

		mainPanel.remove(threePointRedButtonEdit);
		threePointRedButton.setForeground(new Color(0, 0, 0));
		threePointRedButton.setFont(new Font("Arial", Font.BOLD, 20));
		threePointRedButton.setBounds(290, 520, 100, 60);
		mainPanel.add(threePointRedButton);
	}

	@Override
	public void addObserver(FightController controller) {

		this.fightController = controller;
	}

	private class LabelAdapter extends MouseAdapter {
		JWindow wa = new JWindow();

		@Override
		public void mouseEntered(MouseEvent e) {
			JLabel istruzioni = new JLabel();
			istruzioni.setText("<html>Istruzioni/Funzioni da tastiera:"
					+ "<br> Start->T &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Pause->Y"
					+ "<br>1 punto rosso->A	&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 	1 punto blu->K"
					+ "<br>3 punti rosso->z	&nbsp &nbsp &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 3 punti blu->M"
					+ "<br>Ammonizione rosso->Q  &nbsp &nbsp &nbsp &nbsp  Ammonizione blu->O");
			wa.add(istruzioni);
			istruzioni.setFont(new Font("Arial", Font.BOLD, 30));
			wa.setSize(100, 100);
			wa.setLocation(300, 300);
			wa.pack();
			wa.setVisible(true);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			wa.setVisible(false);
		}
	}
}