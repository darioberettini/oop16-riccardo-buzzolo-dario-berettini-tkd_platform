package view;

import controller.FightController;

public interface FightPanel {
	public String getFirstAthleteSurname();

	public String getSecondAthleteSurname();

	public void addObserver(FightController controller);
}
