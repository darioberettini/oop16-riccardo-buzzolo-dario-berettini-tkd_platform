package view;

import java.net.URL;

import controller.FightController;

public interface FightStartedPanel {

	public void endFightMessage(String message);

	public void setSecondsFightText(String newValue);

	public void setSecondsPauseText(String newValue);

	public void setRoundText(Integer newValue);

	public void setBluePoint(URL unit, URL dozen);

	public void setRedPoint(URL unit, URL dozen);

	public void setBlueWarnings(URL warning, int index);

	public void setRedWarnings(URL warning, int index);

	public void addObserver(FightController controller);

	public void updateButtonState(int state);

	public void setOnePointButtonLabel(String newValue);

	public void setThreePointButtonLabel(String newValue);

	public void removeWarning(int color, int index, int coordinates);
}
