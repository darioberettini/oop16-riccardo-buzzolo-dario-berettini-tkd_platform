package view;

import controller.FormController;

public interface FormPanel {

	public void addObserver(FormController controller);
}
