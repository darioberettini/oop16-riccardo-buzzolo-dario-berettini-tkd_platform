package view;

import java.awt.Font;
import java.sql.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import controller.DBConnection;

public class AddUser extends JFrame {

	private static final long serialVersionUID = 1L;
	private DBConnection dataSource;
	private static final String DB_SEARCH_USER = "SELECT * FROM credenzialiutenti.account";
	private JPanel mainPanel = new JPanel();
	private final JLabel title = new JLabel("Nuovo Utente");
	private JLabel user = new JLabel("Username:");
	private JTextField userText = new JTextField();
	private JLabel pass = new JLabel("Password:");
	private JPasswordField passText = new JPasswordField();
	private JButton cancel = new JButton("Annulla");
	private JButton submit = new JButton("OK");

	public AddUser() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.setBounds(100, 100, 500, 380);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setContentPane(mainPanel);
		mainPanel.setLayout(null);

		title.setBounds(150, 30, 300, 40);
		title.setFont(new Font("Arial", Font.BOLD, 20));
		mainPanel.add(title);

		user.setBounds(40, 100, 100, 40);
		mainPanel.add(user);

		userText.setBounds(150, 100, 300, 40);
		mainPanel.add(userText);

		pass.setBounds(40, 180, 100, 40);
		mainPanel.add(pass);

		passText.setBounds(150, 180, 300, 40);
		mainPanel.add(passText);

		cancel.setBounds(120, 270, 120, 50);
		mainPanel.add(cancel);

		submit.setBounds(280, 270, 120, 50);
		mainPanel.add(submit);

		cancel.addActionListener(e -> {
			dispose();
		});

		submit.addActionListener(e -> {

			this.dataSource = new DBConnection();
			if (userText.getText().length() == 0) {
				JOptionPane.showMessageDialog(null, "Inserire username.");
			} else if (passText.getPassword().length == 0) {
				JOptionPane.showMessageDialog(null, "Inserire password.");
			} else {
				final String un = userText.getText();
				final char[] pss = passText.getPassword();
				final String pw = String.copyValueOf(pss);
				if (addUser(un, pw)) {
					JOptionPane.showMessageDialog(null, "Utente aggiunto.");
					dispose();
				} else {
					JOptionPane.showMessageDialog(null, "Nome utente e/o password gi� esistenti.");
					reset();
				}
			}
		});

		this.setLocationRelativeTo(null);
	}

	private void reset() {
		userText.setText(null);
		passText.setText(null);
	}

	private boolean addUser(final String un, final String ps) {
		try {
			Connection conn = dataSource.getConnection();
			PreparedStatement pst1 = conn.prepareStatement(DB_SEARCH_USER);
			PreparedStatement pst2 = conn.prepareStatement(
					"INSERT INTO `credenzialiutenti`.`account` (`username`, `password`) VALUES ('" + un + "','" + ps + "')");
			ResultSet rs = pst1.executeQuery();

			while (rs.next()) {
				if (un.equals(rs.getString("username")) && ps.equals(rs.getString("password"))) {
					conn.close();
					return false;
				}
			}

			try {
				pst2.executeUpdate();
			} catch (Error e) {
				e.printStackTrace();
				conn.close();
				return false;
			}

			conn.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
