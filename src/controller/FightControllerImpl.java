package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Fight;
import model.MatchEditData;
import view.FightPanel;
import view.FightStartedPanel;
import view.FightStartedPanelImpl;
import view.Historical;
import view.HistoricalImpl;
import javax.swing.Timer;

public class FightControllerImpl implements FightController {

	private Fight modelFight;
	private FightPanel fightPanel;
	private FightStartedPanel startedFight;
	private Timer timerSeconds;
	private Timer timerSecondsPause;

	public FightControllerImpl(Fight modelFight, FightPanel fightPanel) {

		this.modelFight = modelFight;
		this.fightPanel = fightPanel;
		this.fightPanel.addObserver(this);
	}

	public void startFight(final boolean adminLog) {

		String blueAthleteSurname = this.fightPanel.getFirstAthleteSurname();
		String redAthleteSurname = this.fightPanel.getSecondAthleteSurname();
		if (blueAthleteSurname.isEmpty() || redAthleteSurname.isEmpty()) {
			JFrame frame = new JFrame("Errore");
			JOptionPane.showMessageDialog(frame, "Atleta/i non inserito/i.");
		} else if (!blueAthleteSurname.matches("[A-Za-z]+") || !redAthleteSurname.matches("[A-Za-z]+")) {
			JFrame frame = new JFrame("Errore");
			JOptionPane.showMessageDialog(frame, "Inserire solo lettere.");
		} else {
			modelFight.setBlueAthleteSurname(blueAthleteSurname);
			modelFight.setRedAthleteSurname(redAthleteSurname);
			startedFight = new FightStartedPanelImpl(redAthleteSurname, blueAthleteSurname, adminLog);
			startedFight.addObserver(this);
		}
	}

	class TimerFight implements ActionListener {
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (modelFight.isOver().equals("not over")) {
				if (modelFight.getSeconds() <= 5 && modelFight.getSeconds() > 0)
					modelFight.playBeepTwo();
				if (modelFight.getSeconds() == 0 && modelFight.getRound() < modelFight.getMaxround()) {
					startedFight.updateButtonState(1);
					timerSeconds.stop();
					modelFight.playBeepOne();
					modelFight.setSecondsPause(modelFight.getModPauseSeconds());
					startedFight.setSecondsPauseText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSecondsPause())));
					timerSecondsPause.start();
				} else if (modelFight.getSeconds() == 0) {
					modelFight.decrementSeconds();
					startedFight.setSecondsFightText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSeconds())));
					modelFight.playBeepOne();
					timerSeconds.stop();
				}
			} else {
				startedFight.setSecondsFightText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSeconds())));
				modelFight.playBeepOne();
				endFight(false);
			}
			startedFight.setSecondsFightText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSeconds())));
			modelFight.decrementSeconds();

		}
	}

	class TimerPause implements ActionListener {
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (modelFight.isOver().equals("not over")) {
				if (modelFight.getSecondsPause() == 0) {
					startedFight.updateButtonState(6);
					timerSecondsPause.stop();
					modelFight.incrementRound();
					startedFight.setRoundText(modelFight.getRound());
					modelFight.setSeconds(modelFight.getModFightSeconds());
					startedFight.setSecondsFightText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSeconds())));

				}
				startedFight.setSecondsPauseText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSecondsPause())));
				modelFight.decrementSecondsPause();
			} else {
				endFight(false);
			}

		}

	}

	public void startTimerFight() {

		startedFight.updateButtonState(2);
		timerSeconds = new Timer(1000, new TimerFight());
		timerSecondsPause = new Timer(1000, new TimerPause());
		timerSeconds.start();
		modelFight.playBeepOne();

	}

	public void resumeTimerFight() {
		timerSeconds.start();
		modelFight.playBeepOne();
	}

	public void stopTimerFight() {

		timerSeconds.stop();
		startedFight.updateButtonState(3);
	}

	public void printAthletes() {

		Historical historical = new HistoricalImpl();
		modelFight.setListMatch(modelFight.getListMatchFile());
		historical.StampaStoricoMatch(modelFight.getListMatch());
		historical.addObserverFight(this);
	}

	public void blueScoredOne() {
		modelFight.onePointBlue();
		String points[] = modelFight.updateBlueScore();
		startedFight.setBluePoint(FightControllerImpl.class.getResource(points[0]),
				FightControllerImpl.class.getResource(points[1]));

	}

	public void blueScoredThree() {
		modelFight.threePointsBlue();
		String points[] = modelFight.updateBlueScore();
		startedFight.setBluePoint(FightControllerImpl.class.getResource(points[0]),
				FightControllerImpl.class.getResource(points[1]));

	}

	public void redScoredOne() {
		modelFight.onePointRed();
		String points[] = modelFight.updateRedScore();
		startedFight.setRedPoint(FightControllerImpl.class.getResource(points[0]),
				FightControllerImpl.class.getResource(points[1]));

	}

	public void redScoredThree() {
		modelFight.threePointsRed();
		String points[] = modelFight.updateRedScore();
		startedFight.setRedPoint(FightControllerImpl.class.getResource(points[0]),
				FightControllerImpl.class.getResource(points[1]));

	}

	public void updateRedWarning() {

		modelFight.incrementWarningRed();
		startedFight.setRedWarnings(FightControllerImpl.class.getResource(modelFight.updateRedWarning()),
				modelFight.getRedWarning());
	}

	public void updateBlueWarning() {

		modelFight.incrementWarningBlue();
		startedFight.setBlueWarnings(FightControllerImpl.class.getResource(modelFight.updateBlueWarning()),
				modelFight.getBlueWarning());

	}

	public void blueScoredKo() {
		modelFight.blueScoredKo();
	}

	public void redScoredKo() {
		modelFight.redScoredKo();
	}

	public void insertListaMatchFile() {

		modelFight.insertListMatchFile();
	}

	public void getListaMatchFile() {

		modelFight.getListMatchFile();
	}

	public void insertListaMatch(String firstAthleteSurname, String secondAthleteSurname, String result) {

		modelFight.insertListMatch(firstAthleteSurname, secondAthleteSurname, result);
	}

	public void cancelOperation() {
		int op = modelFight.cancelLastOperation();
		if (op == 0) {
			String points[] = modelFight.updateBlueScore();
			startedFight.setBluePoint(FightControllerImpl.class.getResource(points[0]),
					FightControllerImpl.class.getResource(points[1]));
		}
		if (op == 1) {
			String points[] = modelFight.updateRedScore();
			startedFight.setRedPoint(FightControllerImpl.class.getResource(points[0]),
					FightControllerImpl.class.getResource(points[1]));
		}
		if (op == 2) {
			String points[] = modelFight.updateBlueScore();
			startedFight.setBluePoint(FightControllerImpl.class.getResource(points[0]),
					FightControllerImpl.class.getResource(points[1]));
		}
		if (op == 3) {
			String points[] = modelFight.updateRedScore();
			startedFight.setRedPoint(FightControllerImpl.class.getResource(points[0]),
					FightControllerImpl.class.getResource(points[1]));
		}
		if (op == 4) {
			if (modelFight.getBlueWarning() % 2 == 0) {
				startedFight.removeWarning(0, modelFight.getBlueWarning(), 0);
			} else {
				startedFight.removeWarning(0, modelFight.getBlueWarning() - 1, 63);
				startedFight.setBlueWarnings(FightControllerImpl.class.getResource(modelFight.updateBlueWarning()),
						modelFight.getBlueWarning());
			}
		}
		if (op == 5) {
			if (modelFight.getRedWarning() % 2 == 0) {
				startedFight.removeWarning(1, modelFight.getRedWarning(), 0);
			} else {
				startedFight.removeWarning(1, modelFight.getRedWarning() - 1, 63);
				startedFight.setRedWarnings(FightControllerImpl.class.getResource(modelFight.updateRedWarning()),
						modelFight.getRedWarning());
			}
		}
	}

	@SuppressWarnings("deprecation")
	public void editParameters(MatchEditData med) {
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		modelFight.setModFightPauseSeconds(med.getPauseValue());
		modelFight.setModFightSeconds(med.getTimerValue());
		modelFight.setSeconds(med.getTimerValue());
		modelFight.setSecondsPause(med.getPauseValue());
		modelFight.setDeltaOne(med.getOnePointValue());
		modelFight.setDeltaThree(med.getThreePointsValue());
		modelFight.setMaxRound(med.getRoundValue());
		startedFight.setSecondsFightText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSeconds())));
		startedFight.setSecondsPauseText(sdf.format(new Date(0, 0, 0, 0, 0, modelFight.getSecondsPause())));
		startedFight.setOnePointButtonLabel(modelFight.getDeltaOne().toString());
		startedFight.setThreePointButtonLabel(modelFight.getDeltaThree().toString());

	}

	// private di questo metodo
	public void endFight(boolean fromView) {
		if (!fromView) {
			timerSeconds.stop();
			timerSecondsPause.stop();
			startedFight.updateButtonState(6);
			startedFight.endFightMessage(modelFight.getIsOverRes());
			modelFight.reset();
		} else {
			timerSeconds.stop();
			timerSecondsPause.stop();
			modelFight.reset();
			startedFight.updateButtonState(6);
		}
	}

}