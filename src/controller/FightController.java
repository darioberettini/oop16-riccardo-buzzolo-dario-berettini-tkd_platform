package controller;

import model.MatchEditData;

/**
 * 
 * @author Berettini, Buzzolo
 *
 */
public interface FightController {
	/**
	 * 
	 * Open the score-machine JFrame, and check if logged with admin account.
	 * 
	 * @param adminLog
	 */
	public void startFight(final boolean adminLog);

	/**
	 * Start the timer for the round.
	 */
	public void startTimerFight();

	/**
	 * Stop the timer.
	 */
	public void stopTimerFight();

	/**
	 * Blue scored 'one' point
	 */
	public void blueScoredOne();

	/**
	 * Blue scored 'three' points
	 */
	public void blueScoredThree();

	/**
	 * Red scored 'one' point
	 */
	public void redScoredOne();

	/**
	 * Red scored 'three' point
	 */
	public void redScoredThree();

	/**
	 * Resume the fight.
	 */
	public void resumeTimerFight();

	/**
	 * Blue scored a KO.
	 */
	public void blueScoredKo();

	/**
	 * Red scored a KO.
	 */
	public void redScoredKo();

	/**
	 * The View passes a MatchEditData object made of elements from the
	 * JTextFields, then it upgrades the model and return new interface.
	 * 
	 * @param med
	 */
	public void editParameters(MatchEditData med);

	/**
	 * Cancel the last operation (points or warning added).
	 */
	public void cancelOperation();

	/**
	 * Open the historical JFrame with all the fights fought
	 */
	public void printAthletes();

	/**
	 * Get an Array of String containing the path of the red warnings as JPG
	 * 
	 * @param warnings
	 * @return the paths of the warnings
	 */
	public void updateRedWarning();

	/**
	 * Get an Array of String containing the path of the blue warnings as JPG
	 * 
	 * @param warnings
	 * @return the paths of the warnings
	 */
	public void updateBlueWarning();

	/**
	 * Save the elements of the list in the file ListaStorici.dat.
	 */
	public void insertListaMatchFile();

	/**
	 * Get all the elements saved on the file ListaStorici.dat
	 */
	public void getListaMatchFile();

	/**
	 * 
	 * @param atleta1
	 * @param atleta2
	 * @param risultato
	 */

	public void insertListaMatch(String firstAthleteSurname, String secondAthleteSurname, String result);

	/**
	 * When the fight reaches a condition to make it end, it stops , print an
	 * alert with the result and save it on the list.
	 * 
	 * @param fromView
	 *           The parameter needs to check if the end is caused by a condition
	 *           o because the user close the window.
	 */
	public void endFight(boolean fromView);

}
