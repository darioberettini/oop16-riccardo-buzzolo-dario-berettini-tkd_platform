package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private final static DBConnection SINGLETON = new DBConnection();
	private final String dbName = "credenzialiutenti";

	public static DBConnection getInstance() {
		return SINGLETON;
	}

	public Connection getConnection() {
		final String driver = "com.mysql.jdbc.Driver";
		final String dbUri = "jdbc:mysql://localhost:3306/" + dbName;

		final String userName = "root";
		final String password = "root";

		Connection connection = null;
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(dbUri, userName, password);
		} catch (ClassNotFoundException e) {
			new Exception(e.getMessage());
			System.out.println("ErroreCNF" + e.getMessage());
		} catch (SQLException e) {
			new Exception(e.getMessage());
			System.out.println("ErroreSQL" + e.getMessage());
		}
		return connection;
	}
}